#!/bin/sh

# Set folders
symfony_root_folder="/vagrant"
symfony_public_folder="$symfony_root_folder/web"

# The host ip is same as guest ip with last octet equal to 1
host_ip=`echo $1 | sed 's/\.[0-9]*$/.1/'`

# Test if Apache or Nginx is installed
nginx -v > /dev/null 2>&1
NGINX_IS_INSTALLED=$?

# Create Symfony folder if needed
if [ ! -d $symfony_root_folder ]; then
    mkdir -p $symfony_root_folder
fi

sudo chmod -R 775 $symfony_root_folder/app/cache
sudo chmod -R 775 $symfony_root_folder/app/logs
sudo chmod -R 775 $symfony_root_folder/app/console

sed -i "s/('127.0.0.1', 'fe80::1'/('127.0.0.1', '$host_ip', 'fe80::1'/" $symfony_public_folder/app_dev.php
sed -i "s/'127.0.0.1',$/'127.0.0.1', '$host_ip',/" $symfony_public_folder/config.php



# xdebug Config
    cat > $(find /etc/php5 -name xdebug.ini) << EOF
zend_extension=$(find /usr/lib/php5 -name xdebug.so)
xdebug.remote_enable = 1
xdebug.remote_connect_back = 1
xdebug.remote_port = 9000
xdebug.scream=0
xdebug.cli_color=1
xdebug.show_local_vars=1

; var_dump display
xdebug.var_display_max_depth = 5
xdebug.var_display_max_children = 256
xdebug.var_display_max_data = 1024
xdebug.max_nesting_level = 250
EOF

    # APC
    cat > $(find /etc/php5 -name apc.ini) << EOF
extension=$(find /usr/lib/php5 -name apc.so)
apc.enabled = 1
apc.enable_cli = 0
apc.ttl = 3600
apc.user_ttl = 3600
apc.shm_size = 128M
apc.mmap_file_mask = /tmp/apc.XXXXXX
EOF

sudo service php5-fpm restart


if [ $NGINX_IS_INSTALLED -eq 0 ]; then

    echo "
    server {
        listen 80;

        root /vagrant/web;
        index index.html;

        # Make site accessible from ...
        server_name $host_ip.xip.io $4;

        access_log /var/log/nginx/$4-access.log;
        error_log  /var/log/nginx/$4-error.log error;

        charset utf-8;

        location ~ \.php$ {
          try_files /u83rhfuhfhvidfhviudfh38ryu8h.htm @php;
        }

        location / {
          index app.php;
          try_files \$uri \$uri/ /app.php?\$query_string /app_dev.php?\$query_string;
        }

        location @php {
          try_files \$uri \$uri/ =404;
          include /etc/nginx/fastcgi_params;
          fastcgi_pass 127.0.0.1:9000;
          fastcgi_index index.php;
          fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
          fastcgi_intercept_errors on;
          fastcgi_buffer_size 128k;
          fastcgi_buffers 4 256k;
          fastcgi_busy_buffers_size 256k;
          fastcgi_param HTTPS off;
        }

    }
    " > /etc/nginx/sites-available/$4

    sudo ln -s /etc/nginx/sites-available/$4 /etc/nginx/sites-enabled/$4

    sudo rm /etc/nginx/sites-enabled/vagrant

    sudo service nginx restart

fi
